<?php
/**
 * Created by PhpStorm.
 * User: sagas
 * Date: 8/11/14
 * Time: 8:06 AM
 */


/**
 * This is a simple pagination class that outputs simple html pagination *
 * */
class Pagination {
    /**
     * @param integer $totalItems total items represented by pagination
     * @param integer $currentStart current active page
     * @param integer $items items per page
     * @param string $queryParams optional additional parameters on each link
     * @return string a string of anchors representing the pagination
     */
    public static function getPagination($totalItems, $currentStart, $items, $queryParams=''){
        $pagination = '';
        if($items < $totalItems){
            $currentStart = floor(($currentStart + $items) / $items);
            $last = ceil($totalItems/$items);
            $anchorSpan = 12;
            $edgeDisplacement = 3;
            $midRange = floor($anchorSpan/2);
            $interPageEllipsis = '<b>&bull;&bull;&bull;</b>';
            $paginationAnchorList = array();

            if($currentStart > 1){
                array_push($paginationAnchorList, array('First', ("?p=1&n=".$items.$queryParams), false));
                array_push($paginationAnchorList, array('Previous', ("?p=".($currentStart-1)."&n=".$items.$queryParams), false));
            }

            if($last <= $anchorSpan){
                for($i=1; $i<=$last; $i++)
                    array_push($paginationAnchorList, array($i, ("?p=$i&n=".$items.$queryParams), ($currentStart == $i)));
            }
            else {
                if($currentStart == 1 ){
                    for($i=1; $i<=($anchorSpan + $edgeDisplacement); $i++)
                        array_push($paginationAnchorList, array($i, ("?p=$i&n=".$items.$queryParams), ($currentStart == $i)));
                    array_push($paginationAnchorList, array($interPageEllipsis, "#", false));
                    array_push($paginationAnchorList, array($last,("?p=$last&n=".$items.$queryParams), ($currentStart == $last)));
                }
                else if($currentStart <= $anchorSpan){
                    for($i=1; $i<=($anchorSpan); $i++)
                        array_push($paginationAnchorList, array($i, ("?p=$i&n=".$items.$queryParams), ($currentStart == $i)));
                    array_push($paginationAnchorList, array($interPageEllipsis, "#", false));
                    array_push($paginationAnchorList, array($last,("?p=$last&n=".$items.$queryParams), ($currentStart == $last)));
                }
                else if($currentStart == $last){
                    array_push($paginationAnchorList, array($interPageEllipsis, "#", false));
                    for($i=($last - ($anchorSpan + $edgeDisplacement)); $i<=$last; $i++)
                        array_push($paginationAnchorList, array($i, ("?p=$i&n=".$items.$queryParams), ($currentStart == $i)));
                }
                else if(($last - $currentStart) <= $anchorSpan){
                    array_push($paginationAnchorList, array($interPageEllipsis, "#", false));
                    for($i=($last - $anchorSpan); $i<=$last; $i++)
                        array_push($paginationAnchorList, array($i, ("?p=$i&n=".$items.$queryParams), ($currentStart == $i)));
                }
                else if($currentStart > $anchorSpan){
                    array_push($paginationAnchorList, array($interPageEllipsis, "#", false));
                    for($i=($currentStart-$midRange); $i<=$currentStart; $i++)
                        array_push($paginationAnchorList, array($i, ("?p=$i&n=".$items.$queryParams), ($currentStart == $i)));
                    for($i=($currentStart+1); $i<=($currentStart+$midRange-1); $i++)
                        array_push($paginationAnchorList, array($i, ("?p=$i&n=".$items.$queryParams), ($currentStart == $i)));
                    array_push($paginationAnchorList, array($interPageEllipsis, "#", false));
                    array_push($paginationAnchorList, array($last,("?p=$last&n=".$items.$queryParams), ($currentStart == $last)));
                }
            }

            if($currentStart < $last){
                array_push($paginationAnchorList, array('Next', ("?p=".($currentStart+1)."&n=".$items.$queryParams), false));
                array_push($paginationAnchorList, array('Last', ("?p=$last&n=".$items.$queryParams), false));
            }

            foreach($paginationAnchorList as $pageLink)
                $pagination .= self::printHtmlAnchor($pageLink[0],$pageLink[1], $pageLink[2]);
        }

        return $pagination;
    }

    /**
     * @param $text string the label of the anchor tag
     * @param $linkUrl string the url this anchor links to
     * @param bool $active whether to add the class 'active' to this anchor
     * @return string an anchor tag representing the parameters passed
     */
    private static function printHtmlAnchor($text, $linkUrl, $active=false){
        $activeClass = ($active)?'class="active"':'';
        $anchor = "<a href='$linkUrl' $activeClass >$text</a>";
        return $anchor;
    }
}

